import {Component} from '@angular/core';
import {BarcodeScanner, BarcodeScannerOptions} from '@ionic-native/barcode-scanner/ngx';
import {ItemService} from '../services/item.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page
{
    scannedData: {};
    customBarCode: any;
    barcodeScannerOptions: BarcodeScannerOptions;

    constructor(
        private barcodeScanner: BarcodeScanner,
        private itemService: ItemService,
        private router: Router,
    )
    {
        this.barcodeScannerOptions = {
            showTorchButton: true,
            showFlipCameraButton: true
        };
    }

    clearCode()
    {
        try
        {
            this.scannedData = null;
            this.customBarCode = null;
        } catch (e)
        {
            alert(JSON.stringify(e));
        }
    }

    scanCode()
    {
        this.barcodeScanner
            .scan()
            .then(barcodeData =>
            {
                // alert('Barcode data ' + JSON.stringify(barcodeData));
                this.scannedData = barcodeData;
            })
            .catch(err =>
            {
                console.log('Error', err);
            });
    }

    async createItem()
    {
        if (this.scannedData)
        {
            var result = await this.validateEan(this.scannedData['text']);
            if (result)
            {
                this.createKilowatt(this.scannedData['text'], this.scannedData['format']);
            } else
            {
                this.itemService.presentToast('Invalid Bar Code! Please Try Again');
            }
        } else if (this.customBarCode)
        {
            var result = await this.validateEan(this.customBarCode);
            if (result)
            {
                this.createKilowatt(this.customBarCode, 'EAN_13'); // assuming you add a custom ean_13 barcode manually
            } else
            {
                this.itemService.presentToast('Invalid Bar Code! Please Try Again');
            }
        } else
        {
            alert('Could Not Upload Barcode');
        }
    }

    //call item service to make the bar code entry
    createKilowatt(value, format)
    {
        this.itemService.createItem(value, format);
        this.clearCode();
        this.router.navigate(['/tabs/tab2']);
    }

    //validate EAN 13 barcodes. I assumed you will be testing EAN_13 barcodes as that is the type in the photo that you sent me in the email
    validateEan(value)
    {
        var checkSum = value.split('').reduce(function(p, v, i)
        {
            return i % 2 == 0 ? p + 1 * v : p + 3 * v;
        }, 0);

        if (checkSum % 10 != 0)
        {
            return false;
        }
        return true;
    }
}

