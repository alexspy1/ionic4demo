import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ItemService} from '../services/item.service';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit
{
    items: Array<any>;
    blueImageToShowInteger: number = 0;
    blueImgSrc: String = '/assets/batteries/blue_' + this.blueImageToShowInteger + '.png';
    goldImageToShowInteger: number = 0;
    goldImgSrc: String = '/assets/batteries/gold_' + this.goldImageToShowInteger + '.png';
    timesRolledOver: number = 0;
    uploadedBarcodes: number = 0;

    constructor(
        private router: Router,
        public itemService: ItemService
    )
    {

    }

    ionViewDidEnter()
    {
        this.handleBlueItems(false);
    }

    ngOnInit()
    {
        this.items = this.itemService.getItems();
        this.handleBlueItems(false);
    }

    // this is a shortcut item in case you wanted to test quickly and add bar codes fast
    addItem()
    {
        this.itemService.createItem('8851492940013', 'EAN_13');
        this.handleBlueItems(false);
    }

    checkUploadedBarcodes()
    {
        this.uploadedBarcodes = this.itemService.getItems().length - (this.timesRolledOver * 10);
    }

    // this will update the BLUE BAR ONLY. THE USER MUST click the blue bar before the GOLD bar will be updated. AKA useClickEvent must be true to change gold bar
    handleBlueItems(useClickEvent)
    {
        this.checkUploadedBarcodes();
        console.log(this.uploadedBarcodes);

        this.blueImageToShowInteger = Math.floor(this.uploadedBarcodes / 2);

        // only update gold bar if they click
        if (useClickEvent)
        {
            this.handleBlueItemsClickEvent();
        }

        if (this.blueImageToShowInteger >= 5)
        {
            this.blueImageToShowInteger = 5;
        }

        this.blueImgSrc = '/assets/batteries/blue_' + this.blueImageToShowInteger + '.png';
    }

    // see how many times the blue bar has rolled over and subtract it from the number of items
    handleBlueItemsClickEvent()
    {
        if (this.blueImageToShowInteger >= 5 && this.goldImageToShowInteger <= 5)
        {
            this.timesRolledOver += 1;
            this.checkUploadedBarcodes();
            this.blueImageToShowInteger = Math.floor(this.uploadedBarcodes / 2);
            this.goldImageToShowInteger += 1;
            this.handleGoldItems();
            console.log('gold ' + this.goldImageToShowInteger);
        }
    }

    handleGoldItems()
    {
        this.goldImgSrc = '/assets/batteries/gold_' + this.goldImageToShowInteger + '.png';
    }
}
