import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ItemService
{
    //enable duplicate barcodes
    enableDuplicates:boolean = false;

    //the array of bar codes
    items: Array<any> =
        [
        ];

    constructor(        public toastController: ToastController
    ) { }

    async presentToast(string)
    {
        const toast = await this.toastController.create({
            message: string,
            duration: 4000
        });
        toast.present();
    }

    createItem(value, format)
    {
        if(this.enableDuplicates)
        {
            this.addItem(value,format);
        }
        else
        {
            if ( !this.duplicateExists(value))
            {
                this.addItem(value,format);
            }
            else
            {
                this.presentToast('Duplicate Entry Found. Please add a unique Bar Code or allow duplicates');
            }
        }
    }

    addItem(value,format)
    {
        let newId = Math.random().toString(36).substr(2, 5);

        this.items.push({
            'id': newId,
            'value': value,
            'format': format
        });
    }

    duplicateExists(value)
    {
        var duplicateExists = this.items.some(function(el){ return el.value === value});

        if(duplicateExists)
        {
            return true;
        }

        return false;
    }

    getItems()
    {
        return this.items;
    }
}
